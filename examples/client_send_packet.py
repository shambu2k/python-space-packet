import spp
import logging

logging.basicConfig(level=logging.DEBUG)


config = spp.Config(
    routing={
        # (APID, APID Qualifier): Address
        (123, None): "localhost:40123",
        },
    transport=spp.UdpTransport()
    )


client = spp.SpacePacketEntity(config)

space_packet = spp.SpacePacket(
    packet_type=spp.PacketType.TELECOMMAND,
    packet_sec_hdr_flag=False,
    apid=123,
    sequence_flags=spp.SequenceFlags.UNSEGMENTED,
    packet_sequence_count=1,
    packet_data_field=b"This is a test packet!!!"
    )


client.request(
    space_packet,
    space_packet.apid,
    apid_qualifier=None,
    qos=None,
    )
