import space_packet
from space_packet import PacketType, SequenceFlags
from space_packet import UdpTransport
import logging

logging.basicConfig(level=logging.DEBUG)


config = space_packet.Config(
    packet_type=PacketType.TELECOMMAND,
    routing={"RECEIVE": {123: "127.0.0.1:5004"},
             "SEND": {123: "127.0.0.1:5005"}
             },

    transport=UdpTransport()
)


client = space_packet.OctetStringEntity(config)

client.request(
    octet_string=b'Helloworld',
    apid=123,
    sec_hdr_flag=0,
    packet_type=1,
    packet_sequence_count=1
)
