import spp
import logging

logging.basicConfig(level=logging.DEBUG)


config = spp.Config(
    transport=spp.UdpTransport()
    )

server = spp.SpacePacketEntity(config)

server.transport.bind("localhost:40123")

input("Running. Press <Enter> to stop...\n")

server.transport.unbind()
