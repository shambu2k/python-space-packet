

DEFAULT_MAXIMUM_PACKET_LENGTH = 4096


class Config:

    def __init__(
            self,
            maximum_packet_length=DEFAULT_MAXIMUM_PACKET_LENGTH,
            routing=None,
            transport=None):
        self.maximum_packet_length = maximum_packet_length
        self.routing = routing
        self.transport = transport

        self.transport.config = self  # back reference to config in transport
