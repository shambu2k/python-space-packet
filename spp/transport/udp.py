import socket
import threading
import select

from .base import Transport


class UdpTransport(Transport):

    def __init__(self):
        super().__init__()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self._thread = threading.Thread(target=self._incoming_sdu_handler)
        self._thread.kill = False

    def request(self, data, address):
        url = address.split(":")
        url[1] = int(url[1])
        self.socket.sendto(data, tuple(url))

    def bind(self, address):
        url = address.split(":")
        url[1] = int(url[1])
        self.socket.bind(tuple(url))
        self._thread.start()

    def unbind(self):
        self._thread.kill = True
        self._thread.join()
        self.socket.close()

    def _incoming_sdu_handler(self):
        thread = threading.currentThread()
        _socket_list = [self.socket]
        buffer_size = 10 * self.config.maximum_packet_length

        while not thread.kill:
            try:
                readable, _, _ = select.select(_socket_list, [], [], 0)
            except ValueError:
                break

            for sock in readable:
                data, addr = self.socket.recvfrom(buffer_size)
                self.indication(data, addr)
