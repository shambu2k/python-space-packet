from .sdu import SpacePacket


class SpacePacketEntity:

    def __init__(self, config):
        self.config = config
        self.transport = config.transport
        self.transport.indication = lambda x, y: self.sdu_received(x, y)

    def request(self, space_packet, apid, apid_qualifier=None, qos=None):
        address = self.config.routing.get((apid, apid_qualifier))
        self.transport.request(space_packet.encode(), address)

    def indication(self, space_packet, apid, apid_qualifier=None):
        print("Received Space Packet:", space_packet)
        attrs = vars(space_packet)
        print(', '.join("%s: %s" % item for item in attrs.items()))

    def sdu_received(self, data, address):
        space_packet = SpacePacket.decode(data)
        self.indication(space_packet, space_packet.apid)


# class OctetStringEntity:
#     def __init__(self, config):
#         self.config = config
#         self.transport = config.transport
#
#     def request(
#         self, octet_string, apid,
#         sec_hdr_flag, packet_type,
#         packet_sequence_count
#     ):
#         packet = space_packet.SpacePacket(
#             packet_type=packet_type,
#             packet_sec_hdr_flag=sec_hdr_flag,
#             apid=apid,
#             sequence_flags=SequenceFlags.UNSEGMENTED,
#             packet_sequence_count=packet_sequence_count,
#             packet_data_length=len(octet_string)-1,
#             packet_data_field=octet_string,
#         )
#         address = self.config.routing.get("SEND").get(apid)
#         self.transport.request(packet.encode(), address)
#
#     def indication(
#         self, apid, sec_hdr_flag, octet_string=None, data_loss_indicator=None
#     ):
#         address = self.config.routing.get("RECEIVE").get(apid)
#         self.transport.bind(address)
