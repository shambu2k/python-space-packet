# Python Space Packet

To be written...

## Installation

Clone the repository and then install via pip:

```
$ git clone https://gitlab.com/librecube/prototypes/python-space-packet
$ cd python-cfdp
$ virtualenv venv
$ . venv/bin/activate
$ pip install -e .
```

## Example

Start the remote entity (server) first:

```
$ cd examples
$ python server.py
```

Run one of the local entity (client) examples:

```
$ cd examples
$ python client_send_packet.py
```

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/python-space-packet/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/python-space-packet

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
